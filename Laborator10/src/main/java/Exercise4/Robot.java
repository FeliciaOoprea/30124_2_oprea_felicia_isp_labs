package Exercise4;

/*Create a Robot class which simulate a robot which moves randomly in a 2D space (with a limited dimension of 100×100).
        Add in the 2D space 10 robots and start them.
        If at a particular point 2 robots arrives on the same location will collide and both of them are destroyed.
        The robots change their position at each 100 milliseconds.
*/
public class Robot {
    private int x;
    private int y;

    public Robot(int x, int y) {
        if (x <= 100) {
            this.x = x;
        }
        if (y <= 100) {
            this.y = y;
        }
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getY() {
        return y;
    }

    public int getX() {
        return x;
    }

    @Override
    public String toString() {
        return "Robot{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }
}
