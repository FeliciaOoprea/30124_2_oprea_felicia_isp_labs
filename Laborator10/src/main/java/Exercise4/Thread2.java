package Exercise4;

import java.util.Random;

public class Thread2 extends Thread {
    private Robot[] robotList;
    int n;
    int x;
    int y;
    Random random = new Random();

    public Thread2(Robot[] robotList, int n) {
        this.robotList = robotList;
        this.n = n;
    }

    @Override
    public void run() {
        while (n > 1) {

            try {
                Thread2.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            for (int i = 1; i < n; i++) {
                if (robotList[i] != null) {
                    x = random.nextInt(10);
                    y = random.nextInt(10);
                    robotList[i].setX(x);
                    robotList[i].setY(y);
                }
            }
        }
    }
}
