package Exercise4;

public class Thread1 extends Thread {
    private Robot[] robotList;
    int n;

    public Thread1(Robot[] robotList, int n) {
        this.robotList = robotList;
        this.n = n;
    }


    @Override
    public void run() {
        while (n > 1) {

            for (int i = 0; i < n; i++) {
                for (int j = i + 1; j < n - 1; j++) {
                    if ((robotList[i] != null && robotList[j] != null) &&
                            robotList[i].getX() == robotList[j].getX() &&
                            robotList[i].getY() == robotList[j].getY()) {
                        System.out.println("2 robots have collided in the point " +
                                robotList[i].getX() + " and " + robotList[i].getY());
                        for (int k = i; k < n - 1; k++) {
                            robotList[k] = robotList[k + 1];
                        }
                        for (int k = j; k < n - 2; k++) {
                            robotList[k] = robotList[k + 1];
                        }
                        n = n - 2;
                    }

                }

            }
            System.out.println("There are " + n + " robots left!");
        }
        System.out.println("There is just one robot left!");
    }
}

