package Exercise4;
/*Create a Robot class which simulate a robot which moves randomly in a 2D space (with a limited dimension of 100×100).
        Add in the 2D space 10 robots and start them.
        If at a particular point 2 robots arrives on the same location will collide and both of them are destroyed.
        The robots change their position at each 100 milliseconds.
*/

import java.util.Random;

public class Main {
    public static void main(String[] args) throws InterruptedException {
        Random random = new Random();
        Robot[] robotList = new Robot[10];
        int x;
        int y;
        for (int i = 0; i < 10; i++) {
            x = random.nextInt(10);
            y = random.nextInt(10);
            robotList[i] = new Robot(x, y);
            System.out.println(robotList[i].toString());
            System.out.println("");
        }
        Thread2 thread2 = new Thread2(robotList,10);
        Thread1 thread1 = new Thread1(robotList, 10);
        thread1.start();
        thread2.start();




    }
}

