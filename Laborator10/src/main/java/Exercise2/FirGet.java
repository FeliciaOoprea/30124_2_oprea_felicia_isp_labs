package Exercise2;

public class FirGet extends Thread{
    Point p;

    public FirGet(Point p){
        this.p = p;
    }

    public void run(){
        int i=0;
        int a,b;
        while(++i<15){
             synchronized(p){
            a= p.getX();
            try {
                sleep(50);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            b = p.getY();
             }
            System.out.println("Am citit: ["+a+","+b+"]");
        }
    }
}
