package Exercise3;

public class Exercise3 {
    public static void main(String[] args) {
        Counter1 counter1 = new Counter1();
        Counter2 counter2 = new Counter2(counter1);
        counter2.start();
        counter1.start();
    }
}
