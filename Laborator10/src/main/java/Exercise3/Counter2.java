package Exercise3;

public class Counter2 extends Thread{
    private Counter1 counter1;
    public Counter2(Counter1 counter1) {
        this.counter1 = counter1;
    }

    @Override
    public void run()  {
        try {
            counter1.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        int i = 100;
        while (++i < 200) {
            System.out.println(this.getName() + " i=" + i);
        }
    }
}
