package Exercise1;

public class Main {
    public static void main(String[] args) {
        BankAccount bankAccount1 = new BankAccount("Owner1", 100);
        BankAccount bankAccount2 = new BankAccount("Owner2", 150);

        bankAccount1.deposit(50);
        System.out.println(bankAccount1.getBalance());
        bankAccount1.getOwner();
        bankAccount1.withdraw(1);
        System.out.println(bankAccount1.toString());

         if (bankAccount1.equals(bankAccount2)) System.out.println(bankAccount1.toString() + "and " +bankAccount2.toString() + " are equals.");
         else System.out.println(bankAccount1.toString() + " and " + bankAccount2.toString() + " are not equals!");

    }
}
