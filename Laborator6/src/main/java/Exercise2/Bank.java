package Exercise2;

import Exercise1.BankAccount;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Bank {
    private final List<BankAccount> bankAccountList;

    public Bank() {
        this.bankAccountList = new ArrayList<>();
    }

    public void addAccount(String owner, double balance) {
        this.bankAccountList.add(new BankAccount(owner, balance));
    }

    public void printAccounts() {
        this.bankAccountList.stream()
                .sorted(Comparator.comparingDouble(BankAccount::getBalance))
                .forEach(System.out::println);
    }

    public void printAccounts(double minimumBalance, double maximumBalance) {
        this.bankAccountList.stream()
                .filter(bankAccount -> bankAccount.getBalance() >= minimumBalance && bankAccount.getBalance() <= maximumBalance)
                .sorted(Comparator.comparingDouble(BankAccount::getBalance))
                .forEach(System.out::println);
    }

    public BankAccount getAccount(String owner) {
        return  bankAccountList.stream()
                .filter(bankAccount -> bankAccount.getOwner().equals(owner))
                .findFirst().get();
    }


}
