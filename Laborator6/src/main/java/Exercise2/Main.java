package Exercise2;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Bank bank = new Bank();
        Scanner scanner  = new Scanner(System.in);
        String name;
        double balance;
        double min, max;
        int option;
        boolean menu  = true;
        while(menu){
            System.out.println();
            System.out.println("Choose an option:\n 1. Add an account. \n " +
                    "2. Print all accounts. \n " +
                    "3. Print all accounts between minimum and maximum balance. \n " +
                    "4. Get account using owners name. \n " +
                    "5. Exit. \n ");
            option = scanner.nextInt();
            switch (option) {
                case 1:
                    System.out.println("Owner name:");
                    name = scanner.next();
                    System.out.println("Balance:");
                    balance = scanner.nextDouble();
                    bank.addAccount(name,balance);
                    break;

                case 2:
                    bank.printAccounts();
                    break;

                case 3:
                    System.out.println("Minimum balance: ");
                    min = scanner.nextDouble();
                    System.out.println("Maximum balance:");
                    max = scanner.nextDouble();
                    System.out.println("Bank account with balance between:" + min + " and " + max);
                    bank.printAccounts(min, max);
                    break;

                case 4:
                    System.out.println("Owners name:");
                    name = scanner.next();
                    System.out.println("The account with owners name: " + name);
                    System.out.println(bank.getAccount(name));
                    break;

                case 5:
                    menu = false;
                    break;

                default:
                    System.out.println("Wrong option!");
            }
        }


}

}
