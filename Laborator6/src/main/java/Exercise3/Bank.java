package Exercise3;

import java.util.Set;
import java.util.TreeSet;

public class Bank {

    private final Set<BankAccount> bankAccountTreeSet;

    public Bank() {
        this.bankAccountTreeSet = new TreeSet<>();
    }

    public void addAccount(String owner, double balance) {
        this.bankAccountTreeSet.add(new BankAccount(owner, balance));
    }

    public void printAccounts() {
        this.bankAccountTreeSet
                .forEach(System.out::println);
    }

    public void printAccounts(double minimumBalance, double maximumBalance) {
        this.bankAccountTreeSet.stream()
                .filter(bankAccount -> bankAccount.getBalance() >= minimumBalance && bankAccount.getBalance() <= maximumBalance)
                .forEach(System.out::println);
    }

    public BankAccount getAccount(String owner) {
        return bankAccountTreeSet.stream()
                .filter(bankAccount -> bankAccount.getOwner().equals(owner))
                .findFirst().get();

    }


}


