package Exercise4;

import java.util.Scanner;

public class ConsoleMenu {
    public static void main(String[] args) {
        Scanner scanner  = new Scanner(System.in);
        Dictionary dictionary = new Dictionary();
        String word, definition;
        int option;
        boolean menu = true;
        while (menu) {
            System.out.println();
            System.out.println("Choose an option: \n " +
                    "1. Add word to the dictionary. \n " +
                    "2. Get the definition of a word. \n " +
                    "3. Get all the words from the dictionary. \n " +
                    "4. Get all the definition from the dictionary. \n " +
                    "5. Exit.");
            option = scanner.nextInt();
            switch(option) {
                case 1:
                    System.out.println("Enter the word: ");
                    word = scanner.next();
                    Word word1 = new Word(word);
                    System.out.println("Enter the definition: ");
                    definition = scanner.next();
                    Definition def = new Definition(definition);
                    dictionary.addWord(word1,def);
                    break;

                case 2:
                    System.out.println("The word for which you want the definition:");
                    word = scanner.next();
                    Word word2 = new Word(word);
                    System.out.println(dictionary.getDefinition(word2));
                    break;

                case 3:
                    System.out.println("All the words from the dictionary:");
                    System.out.println(dictionary.getAllWords());
                    break;

                case 4:
                    System.out.println("All the definitions from the dictionary:");
                    System.out.println(dictionary.getAllDefinitions());
                    break;

                case 5 :
                    menu = false;
                    break;

                default:
                    System.out.println("Wrong option!");

            }

        }

}
}