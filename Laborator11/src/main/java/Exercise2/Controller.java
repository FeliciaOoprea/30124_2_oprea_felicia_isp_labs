package Exercise2;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.Vector;

public class Controller extends JFrame {
    private static Interface interf;
    private static final List<Product> productVector = new Vector<>(0);

    public static void main(String[] args) {

        Interface interf = new Interface();
        interf.getAddButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String name;
                int price;
                int quantity;
                name = interf.getNameTextField().getText();
                price = Integer.parseInt(interf.getPriceTextField().getText());
                quantity = Integer.parseInt(interf.getQuantityTextField().getText());
                productVector.add(new Product(name, price, quantity));
            }

        });
        interf.getViewButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                interf.getProducts().setListData((Vector<? extends Product>) productVector);
            }
        });
        interf.getDeleteButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                productVector.remove(interf.getProducts().getSelectedValue());
                interf.getProducts().setListData((Vector<? extends Product>)productVector);
            }
        });
        interf.getQuantityButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                productVector.get(interf.getProducts().getSelectedIndex()).setQuantity(Integer.parseInt(interf.getNewQuantityTextField().getText()));
                interf.getProducts().setListData((Vector<? extends Product>)productVector);
            }
        });

    }

}
