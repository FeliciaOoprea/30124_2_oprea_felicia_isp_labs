package Exercise2;
//Implement a stock management application in Java.
//       Product properties are: name, quantity, price.
//        The application provide following functionalities:
//        add new product, view available products, delete product, change product available quantity.
//        For implementing the application it will be used the mvc design pattern.

public class Product {
    private String name;
    private int quantity;
    private int price;

    public Product(String name, int price, int quantity) {
        this.name = name;
        this.price = price;
        this.quantity  = quantity;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Override
    public String toString() {
        return "Product{" +
                "name='" + name + '\'' +
                ", quantity=" + quantity +
                ", price=" + price +
                '}';
    }
}


