package Exercise2;
//Implement a stock management application in Java.
//       Product properties are: name, quantity, price.
//        The application provide following functionalities:
//        add new product, view available products, delete product, change product available quantity.
//        For implementing the application it will be used the mvc design pattern.
import javax.swing.*;
import java.util.Vector;

public class Interface extends JFrame {
    private JButton addButton;
    private JButton deleteButton;
    private JButton quantityButton;
    private JButton viewButton;
    private JLabel nameLabel;
    private JLabel quantityLabel;
    private JLabel priceLabel;
    private JLabel newQuantityLabel;
    private JTextField nameTextField;
    private JTextField priceTextField;
    private JTextField quantityTextField;
    private JTextField newQuantityTextField;
    private JList<Product> products;

    public Interface(){
        setTitle("Stock management application");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
        setSize(500,500);
        setResizable(false);
        init();
        this.repaint();
    }

    public void init(){
        this.setLayout(null);
        nameLabel = new JLabel("Product name:");
        nameLabel.setBounds(30,30,100,30);
        nameTextField = new JTextField();
        nameTextField.setBounds(150,30,160,30);

        priceLabel = new JLabel("Price:");
        priceLabel.setBounds(30,60,100,30);
        priceTextField = new JTextField();
        priceTextField.setBounds(150,60,160,30);

        quantityLabel = new JLabel("Quantity:");
        quantityLabel.setBounds(30,90,100,30);
        quantityTextField = new JTextField();
        quantityTextField.setBounds(150,90,160,30);

        addButton = new JButton("Add product");
        addButton.setBounds(320,60,140,30);
        add(addButton);
        add(nameLabel);
        add(nameTextField);
        add(priceLabel);
        add(priceTextField);
        add(quantityLabel);
        add(quantityTextField);

        viewButton = new JButton("Show product list");
        viewButton.setBounds(150,125,160,30);
        add(viewButton);

        products = new JList<>();
        products.setBounds(30,170,430,170);
        add(products);

        newQuantityLabel = new JLabel("New quantity: ");
        newQuantityLabel.setBounds(30,360,150,30);
        newQuantityTextField  = new JTextField();
        newQuantityTextField.setBounds(140,360,140,30);
        quantityButton = new JButton("Change quantity");
        quantityButton.setBounds(310,360,150,30);
        add(newQuantityLabel);
        add(newQuantityTextField);
        add(quantityButton);

        deleteButton = new JButton("Delete product");
        deleteButton.setBounds(310,400,150,30);
        add(deleteButton);


    }

    public JButton getAddButton() {
        return addButton;
    }

    public void setAddButton(JButton addButton) {
        this.addButton = addButton;
    }

    public JButton getDeleteButton() {
        return deleteButton;
    }

    public void setDeleteButton(JButton deleteButton) {
        this.deleteButton = deleteButton;
    }

    public JButton getQuantityButton() {
        return quantityButton;
    }

    public void setQuantityButton(JButton quantityButton) {
        this.quantityButton = quantityButton;
    }

    public JButton getViewButton() {
        return viewButton;
    }

    public void setViewButton(JButton viewButton) {
        this.viewButton = viewButton;
    }

    public JLabel getNameLabel() {
        return nameLabel;
    }

    public void setNameLabel(JLabel addLabel) {
        this.nameLabel = addLabel;
    }

    public JLabel getQuantityLabel() {
        return quantityLabel;
    }

    public void setQuantityLabel(JLabel quantityLabel) {
        this.quantityLabel = quantityLabel;
    }

    public JTextField getAddTextField() {
        return nameTextField;
    }

    public void setAddTextField(JTextField addTextField) {
        this.nameTextField = addTextField;
    }

    public JTextField getNewQuantityTextField() {
        return newQuantityTextField;
    }

    public void setNewQuantityTextField(JTextField quantityTextField) {
        this.newQuantityTextField = quantityTextField;
    }

    public JList<Product> getProducts() {
        return products;
    }



    public JLabel getPriceLabel() {
        return priceLabel;
    }

    public void setPriceLabel(JLabel priceLabel) {
        this.priceLabel = priceLabel;
    }

    public JLabel getNewQuantityLabel() {
        return newQuantityLabel;
    }

    public void setNewQuantityLabel(JLabel newQuantityLabel) {
        this.newQuantityLabel = newQuantityLabel;
    }

    public JTextField getNameTextField() {
        return nameTextField;
    }

    public void setNameTextField(JTextField nameTextField) {
        this.nameTextField = nameTextField;
    }

    public JTextField getPriceTextField() {
        return priceTextField;
    }

    public void setPriceTextField(JTextField priceTextField) {
        this.priceTextField = priceTextField;
    }

    public JTextField getQuantityTextField() {
        return quantityTextField;
    }

    public void setQuantityTextField(JTextField quantityTextField) {
        this.quantityTextField = quantityTextField;
    }

    public void setProducts(JList<Product> products) {
        this.products = products;
    }
}
