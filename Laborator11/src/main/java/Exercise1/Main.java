package Exercise1;

public class Main {
    public static void main(String[] args) {

        Sensor sensor = new Sensor();
        Interface interf = new Interface();
        sensor.register(interf);

        new Thread(sensor).start();
    }
}
