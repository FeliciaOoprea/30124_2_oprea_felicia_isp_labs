package Exercise1;

public class Sensor extends Observable implements Runnable {

    int ok = 1;

    @Override
    public void run() {
        while (ok == 1) {
            this.changeState((int) (Math.random() * 100));

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {

                e.printStackTrace();
            }
        }
    }
}
