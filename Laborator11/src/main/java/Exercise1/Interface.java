package Exercise1;

import javax.swing.*;

public class Interface extends JFrame implements Observer{
    public JTextField temperature;

    Interface() {
        setTitle("Temperature");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setResizable(false);
        setSize(200, 200);

        temperature = new JTextField("");
        add(temperature);
        setVisible(true);
    }

    @Override
    public void update(Object event) {

        temperature.setText("  Temperature: " + event);
    }
}
