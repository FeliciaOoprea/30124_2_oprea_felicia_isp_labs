package Ex4;

import Exercise4.MyPoint;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class testMyPoint {
    @Test
    void testConstructor1() {
        MyPoint p1 = new MyPoint();
        assertEquals(0, p1.getX());
        assertEquals(0, p1.getY());

    }

    @Test
    void testConstructor2() {
        MyPoint p2 = new MyPoint(1, 1);
        assertEquals(1, p2.getX());
        assertEquals(1, p2.getY());

    }

    @Test
    void testSet() {
        MyPoint p3 = new MyPoint();
        p3.setX(2);
        p3.setY(4);
        assertEquals(2, p3.getX());
        assertEquals(4, p3.getY());
        p3.setXY(5, 5);
        assertEquals(5, p3.getX());
        assertEquals(5, p3.getY());

    }

    @Test
    void testDistance1() {
        MyPoint p4 = new MyPoint(3, 3);
        assertEquals(1.41, p4.distance(4, 4), 0.1);
    }

    @Test
    void testDistance2(){
        MyPoint p5 = new MyPoint(4, 4);
        MyPoint p6 = new MyPoint(2,2);
        assertEquals(2.82, p5.distance(p6),0.1);
    }
}
