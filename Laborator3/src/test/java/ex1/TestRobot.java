package Ex1;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class testRobot {
    @Test
    public void testConstructor() {
        Robot robot = new Robot();
        assertEquals(1, robot.getX());
    }

    @Test
    public void testChangeHigherThen1() {
        Robot robot = new Robot();
        assertEquals(1, robot.getX());
        robot.change(10);
        assertEquals(11, robot.getX());
    }

    @Test
    public void testChangeLowerThan1() {
        Robot robot = new Robot();
        assertEquals(1, robot.getX());
        robot.change(0);
        assertEquals(1, robot.getX());
    }
}
