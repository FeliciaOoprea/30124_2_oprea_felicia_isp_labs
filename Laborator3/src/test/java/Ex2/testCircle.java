package Ex2;

import Exercise2.Circle;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class testCircle {
    @Test
    public void testConstructor1() {
        Circle circle1 = new Circle(1);
        assertEquals(1.0, circle1.getRadius(), 0.1);
    }


    @Test
    public void testConstructor2() {
        Circle circle2 = new Circle(2, "blue");
        assertEquals(2.0,circle2.getRadius(),0.1);
        assertEquals("blue",circle2.getColor());
    }

    @Test
    public void testArea(){
        Circle circle3 = new Circle(1.0);
        assertEquals(3.14, circle3.getArea(),0.1);
    }
}
