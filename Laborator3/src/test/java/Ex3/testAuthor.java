package Ex3;

import Exercise3.Author;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class testAuthor {
    @Test
    void testConstructor1() {
        Author author1 = new Author("Author1", "Author1@gmail.com", 'm');
        assertEquals("Author1", author1.getName());
        assertEquals("Author1@gmail.com", author1.getEmail());
        assertEquals('m', author1.getGender());
    }

    @Test
    void testConstructor2() {
        Author author2 = new Author("Author2", "Author2@gmail.com", 'f');
        assertEquals("Author2", author2.getName());
        assertEquals("Author2@gmail.com", author2.getEmail());
        assertEquals('f', author2.getGender());
    }

    @Test
    void testConstructor3() {
        Author author3 = new Author("Author3", "Author3@gmail.com", 'x');
        assertEquals("Author3", author3.getName());
        assertEquals("Author3@gmail.com", author3.getEmail());
        assertEquals(' ', author3.getGender());

    }


    @Test
    void testSetEmail(){
        Author author4 = new Author("Author4", "Author4@gmail.com", 'f');
        assertEquals("Author4@gmail.com", author4.getEmail());
        author4.setEmail("newEmailAuthor4@gmail.com");
        assertEquals("newEmailAuthor4@gmail.com",author4.getEmail());

    }


}
