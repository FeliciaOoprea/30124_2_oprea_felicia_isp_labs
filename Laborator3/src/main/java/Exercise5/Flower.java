package Exercise5;

public class Flower {
    int petal;
    private static int flowers = 0;

    public Flower(int petal) {
        this.petal = petal;
        System.out.println("Flower with " + petal + " petals has been created!");
        flowers++;
    }

    public static int getFlowers() {
        return flowers;
    }

    public static void main(String[] args) {
        Flower f1 = new Flower(3);
        Flower f2 = new Flower(5);
        Flower f3 = new Flower(3);
    }
}
