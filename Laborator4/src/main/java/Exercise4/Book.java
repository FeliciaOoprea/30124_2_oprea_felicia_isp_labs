package Exercise4;

import Exercise2.Author;



public class Book {
    private final String name;
    private double price;
    private final Author[] authors;
    private int qtyInStock = 0;

    public Book(String name, Author[] authors, double price) {
        this.name = name;
        this.authors = authors;
        this.price = price;
    }

    public Book(String name, Author[] authors, double price, int qtyInStock) {
        this.name = name;
        this.authors = authors;
        this.price = price;
        this.qtyInStock = qtyInStock;
    }

    public String getName() {
        return name;
    }

    public Author[] getAuthors() {
        return authors;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getQtyInStock() {
        return qtyInStock;
    }

    public void setQtyInStock(int qtyInStock) {
        this.qtyInStock = qtyInStock;
    }

    @Override
    public String toString() {
        return name + " by " + authors.length + " authors.";
    }

    public void printAuthors() {
        System.out.println("Authors:");
        for (Author author : authors) {
            System.out.println(author);

        }
    }


    public static void main(String[] args) {
        Author[] authors = new Author[2];
        authors[0] = new Author("Author1", "author1@gmail.com", 'm');
        authors[1] = new Author("Author2", "author2@gmail.com", 'f');
        Book book = new Book("book1", authors, 200, 10);
        System.out.println(book.toString());
        book.printAuthors();

    }

}
