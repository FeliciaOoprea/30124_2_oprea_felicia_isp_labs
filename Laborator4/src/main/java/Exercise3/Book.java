package Exercise3;

import Exercise2.Author;

public class Book {
    private final String name;
    private double price;
    private final Author author;
    private int qtyInStock = 0;

    public Book(String name, Author author, double price) {
        this.name = name;
        this.author = author;
        this.price = price;
    }

    public Book(String name, Author author, double price, int qtyInStock){
        this.name = name;
        this.author = author;
        this.price = price;
        this.qtyInStock=qtyInStock;
    }

    public String getName() {
        return name;
    }

    public Author getAuthor() {
        return author;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getQtyInStock() {
        return qtyInStock;
    }

    public void setQtyInStock(int qtyInStock) {
        this.qtyInStock = qtyInStock;
    }

    @Override
    public String toString() {
        return  "'" + name + "'" + " by " + author.toString();
    }

    public static void main(String[] args) {
        Author AnneFrank = new Author("Anne Frank","Anne@gmail.com",'f');
        Book book = new Book("The Diary of a Young Girl",AnneFrank,30,100);
        System.out.println(book.toString());
    }
}
