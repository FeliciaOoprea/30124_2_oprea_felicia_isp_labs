package Exercise5;

import Exercise1.Circle;

public class Cylinder extends Circle {
    private double height ;

    public Cylinder() {
    }

    public Cylinder(double radius) {
        super(radius);
        this.height = 1.0;
    }

    public Cylinder(double radius, double height) {
        super(radius);
        this.height = height;
    }

    public double getHeight() {
        return height;
    }

    public double getVolume() {
        return Math.PI * Math.pow(getRadius(), 2) * height;
    }

    @Override
    public double getArea() {
        return 2 * super.getArea() + 2 * Math.PI * getRadius() * getHeight();
    }
}

