package Ex3;

import Exercise2.Author;
import Exercise3.Book;
import org.junit.jupiter.api.Test;
import static  org.junit.jupiter.api.Assertions.assertEquals;

public class testBook {
    @Test
    void testConstructor1(){
        Author author1 = new Author("Author1", "author1@gmail.com", 'f');
        Book book1 = new Book("book1", author1, 5);
        assertEquals("book1", book1.getName());
        assertEquals(author1, book1.getAuthor());
        assertEquals(5,book1.getPrice(),0.1);
    }

    @Test
    void testConstructor2(){
        Author author2 = new Author("Author2", "author2@gmail.com", 'f');
        Book book2 = new Book("book2", author2, 10, 100);
        assertEquals("book2", book2.getName());
        assertEquals(author2, book2.getAuthor());
        assertEquals(10,book2.getPrice(),0.1);
        assertEquals(100, book2.getQtyInStock(),0.1);
    }

}
