package Ex1;

import Exercise1.Circle;
import org.junit.jupiter.api.Test;


import static org.junit.jupiter.api.Assertions.assertEquals;

public class testCircle {
    @Test
    public void testConstructor1(){
        Circle circle = new Circle();
        assertEquals(1.0,circle.getRadius());
    }

    @Test
    public void testConstructor2() {
        Circle circle = new Circle(1);
        assertEquals(1.0, circle.getRadius(), 0.1);
    }


    @Test
    public void testConstructor3() {
        Circle circle = new Circle(2, "blue");
        assertEquals(2.0,circle.getRadius(),0.1);
        assertEquals("blue",circle.getColor());
    }

    @Test
    public void testArea(){
        Circle circle3 = new Circle(1.0);
        assertEquals(3.14, circle3.getArea(),0.1);
    }
}
