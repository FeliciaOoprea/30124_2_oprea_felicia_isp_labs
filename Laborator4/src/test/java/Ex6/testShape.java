package Ex6;

import Exercise6.Shape;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class testShape {
    @Test
    public void testConstructor1(){
        Shape shape1=new Shape();
        assertEquals("green",shape1.getColor());
        assertTrue(shape1.isFilled());
    }

    @Test
    public void testConstructor2(){
        Shape shape2=new Shape("blue",false);
        assertEquals("blue",shape2.getColor());
        assertFalse(shape2.isFilled());
    }
}
