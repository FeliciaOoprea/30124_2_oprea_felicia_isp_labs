package Ex6;
import Exercise6.Square;
import org.junit.jupiter.api.Test;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class testSquare {
    @Test
    public void testConstructor1(){
        Square square = new Square();
        assertEquals(1.0,square.getSide());
    }

    @Test
    public void testConstructor2(){
        Square square = new Square(2);
        assertEquals(2.0,square.getSide());
    }

    @Test
    public void testConstructor3(){
        Square square = new Square(2,"red",true);
        assertEquals(2.0,square.getSide());
        assertEquals("red",square.getColor());
        assertTrue(square.isFilled());
    }

    @Test
    public void testPerimeter(){
        Square square = new Square(2);
        assertEquals(8,square.getPerimeter());
    }

    @Test
    public void testArea(){
        Square square = new Square(2);
        assertEquals(4,square.getArea());
    }

    @Test
    public void testSetLength(){
        Square square = new Square();
        square.setLength(2);
        assertEquals(2,square.getSide());
    }

    @Test
    public void testSetWidth(){
        Square square = new Square();
        square.setWidth(2);
        assertEquals(2.0,square.getWidth());
    }

}
