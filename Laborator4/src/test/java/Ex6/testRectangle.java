package Ex6;

import Exercise6.Rectangle;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class testRectangle {
    @Test
    public void testConstructor1(){
        Rectangle rectangle = new Rectangle();
        assertEquals(1.0,rectangle.getLength());
        assertEquals(1.0,rectangle.getWidth());
    }

    @Test
    public void testConstructor2(){
        Rectangle rectangle = new Rectangle(2.0,2.0);
        assertEquals(2.0,rectangle.getLength());
        assertEquals(2.0,rectangle.getWidth());
    }

    @Test
    public void testConstructor3(){
        Rectangle rectangle = new Rectangle(2.0,2.0,"blue",true);
        assertEquals(2.0,rectangle.getLength());
        assertEquals(2.0,rectangle.getWidth());
        assertEquals("blue",rectangle.getColor());
        assertTrue(rectangle.isFilled());
    }

    @Test
    public void testGetPerimeter(){
        Rectangle rectangle = new Rectangle(2,3);
        assertEquals(10,rectangle.getPerimeter());
    }

    @Test
    public void testGetArea(){
        Rectangle rectangle = new Rectangle(2,3);
        assertEquals(6,rectangle.getArea());
    }

}
