package Ex6;

import Exercise6.Circle;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class testCircle {
    @Test
    public void testConstructor1(){
        Circle circle1 = new Circle();
        assertEquals(1.0,circle1.getRadius(),0.1);
    }

    @Test
    public void testConstructor2(){
        Circle circle2 = new Circle(2.0);
        assertEquals(2.0,circle2.getRadius());
    }
    @Test
    public void testGetArea(){
        Circle circle3 = new Circle(2);
        assertEquals(12.56,circle3.getArea(),0.1);
    }



}
