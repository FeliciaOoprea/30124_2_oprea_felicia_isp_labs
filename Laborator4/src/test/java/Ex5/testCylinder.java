package Ex5;

import Exercise5.Cylinder;
import org.junit.jupiter.api.Test;



import static org.junit.jupiter.api.Assertions.assertEquals;

public class testCylinder {
    @Test
    public void testConstructor1() {
        Cylinder cylinder = new Cylinder();
        assertEquals(1.0, cylinder.getHeight());
    }

    @Test
    public void testConstructor2() {
        Cylinder cylinder = new Cylinder(1);
        assertEquals(1.0, cylinder.getHeight());
        assertEquals(1.0, cylinder.getRadius());
    }

    @Test
    public void testConstructor3() {
        Cylinder cylinder = new Cylinder(1.0, 2.0);
        assertEquals(2.0, cylinder.getHeight());
        assertEquals(1.0, cylinder.getRadius());
    }

    @Test
    public void testGetVolume() {
        Cylinder cylinder = new Cylinder(1.0, 1.0);
        assertEquals(3.14, cylinder.getVolume(), 0.1);
    }

    @Test
    public void testArea() {
        Cylinder cylinder = new Cylinder(2, 5);
        assertEquals(87.92, cylinder.getArea(), 0.1);
    }

}
