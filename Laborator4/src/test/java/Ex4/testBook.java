package Ex4;

import Exercise2.Author;
import Exercise4.Book;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class testBook {
    @Test
    void testConstructor1() {
        Author[] authors = new Author[2];
        authors[0] = new Author("Author1", "author1@gmail.com", 'm');
        authors[1] = new Author("Author2", "author2@gmail.com", 'f');
        Book b1 = new Book("book1", authors, 200);
        assertArrayEquals(authors, b1.getAuthors());
        assertEquals("book1", b1.getName());
        assertEquals(200, b1.getPrice());

    }

    @Test
    void testConstructor2() {
        Author[] authors = new Author[3];
        authors[0] = new Author("Author1", "author1@gmail.com", 'm');
        authors[1]  = new Author("Author2", "author2@gmail.com", 'f');
        Book b2 = new Book("book1", authors, 100, 250);
        assertEquals("book1", b2.getName());
        assertArrayEquals(authors, b2.getAuthors());
        assertEquals(100, b2.getPrice());
        assertEquals(250, 250, 0.1);

    }

}
