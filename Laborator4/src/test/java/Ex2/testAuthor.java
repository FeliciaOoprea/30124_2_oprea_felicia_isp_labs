package Ex2;

import Exercise2.Author;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class testAuthor {
    @Test
    void test1Constructor() {
        Author author1 = new Author("Author1", "Author1@gmail.com", 'm');
        assertEquals("Author1", author1.getName());
        assertEquals("Author1@gmail.com", author1.getEmail());
        assertEquals('m', author1.getGender());
    }

    @Test
    void test2Constructor() {
        Author author2 = new Author("Author2", "Author2@gmail.com", 'f');
        assertEquals("Author2", author2.getName());
        assertEquals("Author2@gmail.com", author2.getEmail());
        assertEquals('f', author2.getGender());
    }


}
