package Exercise1;

public class Main {
    public static void main(String[] args) {
        Shape[] array = new Shape[3];
        array[0] = new Circle(2);
        array[1] = new Rectangle(1, 2);
        array[2] = new Square(4);

        for (int i = 0; i < array.length; i++) {
            System.out.println(array[i].toString());
            System.out.println("Area:" + array[i].getArea());
            System.out.println("Perimeter:" + array[i].getPerimeter());

        }
    }
}

