package Exercise2;

public class ProxyImage implements Image {
    private Image realImage;
    private String fileName;
    private boolean rotated;

    public ProxyImage(String fileName, boolean rotated) {
        this.fileName = fileName;
        this.rotated = rotated;
    }

    @Override
    public void display() {
        if (rotated){
            realImage = new RotatedImage(fileName);
        } else {
            realImage = new RealImage(fileName);
        }
        realImage.display();
    }
}
