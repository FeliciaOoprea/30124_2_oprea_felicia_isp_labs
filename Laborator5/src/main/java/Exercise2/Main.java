package Exercise2;

public class Main {
    public static void main(String[] args) {
        RealImage realImage = new RealImage("realImage");
        realImage.display();
        System.out.println('\n');

        RotatedImage rotatedImage = new RotatedImage("rotatedImage");
        rotatedImage.display();
        System.out.println('\n');

        ProxyImage proxyImage = new ProxyImage("proxyImage",true);
        proxyImage.display();



    }
}

