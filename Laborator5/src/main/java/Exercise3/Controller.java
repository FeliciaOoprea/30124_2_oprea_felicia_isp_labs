package Exercise3;

public class Controller {
    private final Sensor temperatureSensor = new TemperatureSensor();
    private final Sensor lightSensor = new LightSensor();

    public void control() throws InterruptedException {
        for (int i = 0; i < 20; i++){
            System.out.println("Temperature sensor value: " + temperatureSensor.readValue());
            System.out.println("Light sensor value: " + lightSensor.readValue());
            Thread.sleep(1000);
        }
    }

    public static void main(String[] args) throws InterruptedException {
        Controller controller = new Controller();
        controller.control();
    }
}
