package Exercise3;

public class TemperatureSensor extends Sensor{
    @Override
    public int readValue() {
        return (int) Values.generateValues();
    }
}
