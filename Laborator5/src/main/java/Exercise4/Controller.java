package Exercise4;

import Exercise3.LightSensor;
import Exercise3.Sensor;
import Exercise3.TemperatureSensor;


public class Controller {
    private final Sensor tempSensor = new TemperatureSensor();
    private final Sensor lightSensor = new LightSensor();

    private static Controller instance;

    private Controller(){

    }

    public static Controller getInstance(){
        if (instance == null){
            instance = new Controller();
        }
        return  instance;
    }

    public void control() throws InterruptedException {
        for (int i = 0; i < 20; i++){
            System.out.println("Temperature sensor value: " + tempSensor.readValue());
            System.out.println("Light sensor value: " + lightSensor.readValue());
            Thread.sleep(1000);
        }
    }

    public static void main(String[] args) throws InterruptedException {
        Exercise3.Controller controller = new Exercise3.Controller();
        controller.control();
    }
}
