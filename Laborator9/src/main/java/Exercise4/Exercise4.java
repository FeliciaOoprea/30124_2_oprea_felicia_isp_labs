package Exercise4;

import javax.swing.*;
import java.awt.*;


public class Exercise4 extends JFrame {
    String currentPlayer = "x";
    boolean hasWinner;
    JButton[] buttons = new JButton[9];
    private int totalGames = 0;
    private int xWins = 0;
    private int oWins = 0;

    public Exercise4() {
        setLayout(new GridLayout(3, 3));
        setVisible(true);
        initializeButtons();
        initializeMenuBar();
    }

    private void initializeMenuBar() {
        JMenuBar menuBar = new JMenuBar();
        JMenu menu = new JMenu("Menu");
        JMenuItem newGame = new JMenuItem("New game");
        newGame.addActionListener(e -> resetBoard());
        JMenuItem history = new JMenuItem("History");
        history.addActionListener(e -> showHistory());
        JMenuItem quit = new JMenuItem("Quit");
        quit.addActionListener(e -> System.exit(0));
        menu.setFont(new Font("Font", Font.ITALIC, 40));
        menuBar.setBackground(Color.gray);

        menu.add(newGame);
        menu.add(history);
        menu.add(quit);
        menuBar.add(menu);
        setJMenuBar(menuBar);
    }

    public void showHistory() {
        JOptionPane.showMessageDialog(null, "Total number of games: " + totalGames + "\n" +
                "Games won by player X: " + xWins + "\n" +
                "Games won by player 0: " + oWins + "\n");
    }

    public void initializeButtons() {
        for (int i = 0; i < 9; i++) {
            buttons[i] = new JButton();
            buttons[i].setText("");
            buttons[i].setFont(new Font("Font", Font.BOLD, 120));
            buttons[i].setBackground(Color.WHITE);
            int finalI = i;
            buttons[i].addActionListener(e -> {
                if (((JButton) e.getSource()).getText().equals("") &&
                        !hasWinner) {
                    buttons[finalI].setText(currentPlayer);
                    hasWinner();
                    switchPlayer();
                    if (!hasWinner) gameOver();
                }
            });

            add(buttons[i]);

        }
    }

    private void gameOver() {
        for (int i = 0; i < 9; i++) {
            if (buttons[i].getText().equals("")) return;
        }
        resetBoard();
        totalGames++;

    }

    private void switchPlayer() {
        if (currentPlayer.equals("x")) {
            currentPlayer = "0";
        } else {
            currentPlayer = "x";
        }
    }

    private void resetBoard() {
        currentPlayer = "x";
        hasWinner = false;
        for (int i = 0; i < 9; i++) {
            buttons[i].setText("");
            buttons[i].setBackground(Color.WHITE);
        }
    }

    private void hasWinner() {
        if (buttons[0].getText().equals(currentPlayer) && buttons[1].getText().equals(currentPlayer) &&
                buttons[2].getText().equals(currentPlayer)) {
            buttons[0].setBackground(Color.cyan);
            buttons[1].setBackground(Color.cyan);
            buttons[2].setBackground(Color.cyan);
            winner();
        }
        if (buttons[3].getText().equals(currentPlayer) && buttons[4].getText().equals(currentPlayer) &&
                buttons[5].getText().equals(currentPlayer)) {
            buttons[3].setBackground(Color.cyan);
            buttons[4].setBackground(Color.cyan);
            buttons[5].setBackground(Color.cyan);
            winner();
        }
        if (buttons[6].getText().equals(currentPlayer) && buttons[7].getText().equals(currentPlayer) &&
                buttons[8].getText().equals(currentPlayer)) {
            buttons[6].setBackground(Color.cyan);
            buttons[7].setBackground(Color.cyan);
            buttons[8].setBackground(Color.cyan);
            winner();

        }
        if (buttons[0].getText().equals(currentPlayer) && buttons[3].getText().equals(currentPlayer) &&
                buttons[6].getText().equals(currentPlayer)) {
            buttons[0].setBackground(Color.cyan);
            buttons[3].setBackground(Color.cyan);
            buttons[6].setBackground(Color.cyan);
            winner();
        }

        if (buttons[1].getText().equals(currentPlayer) && buttons[4].getText().equals(currentPlayer) &&
                buttons[7].getText().equals(currentPlayer)) {
            buttons[1].setBackground(Color.cyan);
            buttons[4].setBackground(Color.cyan);
            buttons[7].setBackground(Color.cyan);
            winner();

        }
        if (buttons[2].getText().equals(currentPlayer) && buttons[5].getText().equals(currentPlayer) &&
                buttons[8].getText().equals(currentPlayer)) {
            buttons[2].setBackground(Color.cyan);
            buttons[8].setBackground(Color.cyan);
            buttons[5].setBackground(Color.cyan);
            winner();

        }
        if (buttons[0].getText().equals(currentPlayer) && buttons[4].getText().equals(currentPlayer) &&
                buttons[8].getText().equals(currentPlayer)) {
            buttons[0].setBackground(Color.cyan);
            buttons[4].setBackground(Color.cyan);
            buttons[8].setBackground(Color.cyan);
            winner();
        }
        if (buttons[2].getText().equals(currentPlayer) && buttons[4].getText().equals(currentPlayer) &&
                buttons[6].getText().equals(currentPlayer)) {
            buttons[2].setBackground(Color.cyan);
            buttons[4].setBackground(Color.cyan);
            buttons[6].setBackground(Color.cyan);
            winner();
        }
    }

    private void winner() {
        JOptionPane.showMessageDialog(null, "Player " + currentPlayer + " has won the game!");
        hasWinner = true;
        resetBoard();
        if (currentPlayer.equals("x")) {
            xWins++;
        } else {
            oWins++;
        }
        totalGames++;

    }

}
