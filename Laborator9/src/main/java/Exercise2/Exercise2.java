package Exercise2;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Exercise2 extends JFrame {
    JButton button;
    JLabel label;
    JTextField textField;
    int counter = 0;

    Exercise2(){
        setTitle("Exercise 2");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(300,200);
        setVisible(true);
    }

    public void init(){
        this.setLayout(null);
        button = new JButton("Counter");
        button.setBounds(80,100,80,20);
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                counter++;
                textField.setText(String.valueOf(counter));
            }
        });

        label = new JLabel("Number of increments:");
        label.setBounds(10,50,150,20);

        textField = new JTextField();
        textField.setBounds(150,50,80,20);

        add(button); add(label);add(textField);
    }

    public static void main(String[] args) {
        new Exercise2();
    }
}
