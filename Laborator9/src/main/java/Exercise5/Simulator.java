package Exercise5;

import javax.swing.*;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;


public class Simulator extends JFrame {
    JButton addButton;
    JLabel trainNameLabel;
    JTextField trainNameField;
    JLabel destinationLabel;
    JTextField destinationField;
    JLabel segmentLabel;
    JTextField segmentField;
    JLabel stationsLabel;
    JTextArea textArea;
    String destination;
    String name;
    int id;
    ArrayList<Controller> controllers = new ArrayList<>();
    Path path = Paths.get("src/main/resources/ex5.txt");


    Simulator() throws IOException {
        setTitle("Exercise5");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(700, 700);
        setVisible(true);
        setResizable(false);
        init();
        createControllers();
        setTextArea();
    }

    public void init() {
        this.setLayout(null);
        trainNameLabel = new JLabel("Train name: ");
        trainNameLabel.setBounds(50, 50, 200, 40);
        trainNameField = new JTextField();
        trainNameField.setBounds(180, 50, 250, 40);

        destinationLabel = new JLabel("Destination: ");
        destinationLabel.setBounds(50, 110, 200, 40);
        destinationField = new JTextField();
        destinationField.setBounds(180, 110, 250, 40);

        segmentLabel = new JLabel("Segment:");
        segmentLabel.setBounds(50, 170, 200, 40);
        segmentField = new JTextField();
        segmentField.setBounds(180, 170, 250, 40);

        stationsLabel = new JLabel("Stations: ");
        stationsLabel.setBounds(50,200,250,40);

        textArea = new JTextArea();
        textArea.setBounds(50, 250, 550, 380);


        addButton = new JButton("Add");
        addButton.setBounds(500, 90, 80, 80);
        addButton.addActionListener(e -> {
            try {
                addTrain();
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }
        });

        add(trainNameLabel);
        add(segmentLabel);
        add(destinationLabel);
        add(destinationField);
        add(trainNameField);
        add(segmentField);
        add(addButton);
        add(textArea);
        this.repaint();
    }

    private void addTrain() throws IOException {
        destination = destinationField.getText();
        name = trainNameField.getText();
        id = Integer.parseInt(segmentField.getText());
        Train t = new Train(destination, name);
        for (Controller c:controllers){
            if (c.getStationName().equals(destination)){
                if(c.arriveTrain(t,id)){
                    String textToAppend ="=== STATION " + destination + " ===\n" +
                            "|----------ID=" + id + "__Train=" + name + " to " +destination + "__----------|\n";
                    Files.write(path, textToAppend.getBytes(), StandardOpenOption.APPEND);
                    textArea.setText("");
                    setTextArea();
                }

            }
        }
        }



    private void setTextArea() throws IOException {
        FileReader fileReader = new FileReader("src/main/resources/ex5.txt");
        int ch;
        while ((ch = fileReader.read()) != -1) {
            textArea.append(Character.toString((char) ch));
        }
    }

    private void createControllers() throws IOException {

        //build station Cluj-Napoca
        Controller c1 = new Controller("Cluj-Napoca");
        controllers.add(c1);
        Segment s1 = new Segment(1);
        Segment s2 = new Segment(2);
        Segment s3 = new Segment(3);
        c1.addControlledSegment(s1);
        c1.addControlledSegment(s2);
        c1.addControlledSegment(s3);

        //build station Bucuresti
        Controller c2 = new Controller("Bucuresti");
        controllers.add(c2);
        Segment s4 = new Segment(4);
        Segment s5 = new Segment(5);
        Segment s6 = new Segment(6);
        c2.addControlledSegment(s4);
        c2.addControlledSegment(s5);
        c2.addControlledSegment(s6);


        //build station Brasov
        Controller c3 = new Controller("Brasov");
        controllers.add(c3);
        Segment s7 = new Segment(7);
        Segment s8 = new Segment(8);
        Segment s9 = new Segment(9);
        c3.addControlledSegment(s7);
        c3.addControlledSegment(s8);
        c3.addControlledSegment(s9);

        //connect the 3 controllers

        c1.setNeighbourController(c2);
        c1.setNeighbourController(c3);
        c2.setNeighbourController(c1);
        c2.setNeighbourController(c3);
        c3.setNeighbourController(c1);
        c3.setNeighbourController(c2);

        //testing
        //Controller 3 is connected with controller 1 and 2, 2 trains from Brasov leave to Cluj-Napoca and Bucuresti
        Train t1 = new Train("Bucuresti", "Train-1");
        s7.arriveTrain(t1);

        Train t2 = new Train("Cluj-Napoca", "Train-2");
        s8.arriveTrain(t2);

        c1.displayStationState();
        c2.displayStationState();
        c3.displayStationState();

    }

    public static void main(String[] args) throws IOException {
        new Simulator();



    }
}
