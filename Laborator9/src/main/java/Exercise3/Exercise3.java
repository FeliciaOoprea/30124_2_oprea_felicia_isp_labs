package Exercise3;
/*Create an application which let a user enter a file name in a text
field and then when a button is pressed to display
the content of the file in a text area./*
 */

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

public class Exercise3 extends JFrame {
    JButton button;
    JLabel labelT;
    JLabel labelB;
    JTextField textField;
    JTextArea textArea;
    String fileName;

    Exercise3(){
        setTitle("Exercise3");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(400,400);
        setVisible(true);
    }
    public void init(){

        this.setLayout(null);
        button  = new JButton("Open");
        button.setBounds(150,100,100,30);
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                fileName = "src/main/resources/" + textField.getText() + ".txt";
                try { int ch;
                    FileReader fileReader = new FileReader(fileName);
                    while ((ch =  fileReader.read()) != -1) {
                        textArea.append(Character.toString((char)ch));
                    }
                } catch (FileNotFoundException fileNotFoundException) {
                    fileNotFoundException.printStackTrace();
                } catch (IOException ioException) {
                    ioException.printStackTrace();
                }
            }
        });

        labelT = new JLabel("Text:");
        labelT.setBounds(40,160,30,20);

        labelB = new JLabel("File name:");
        labelB.setBounds(70,50,60,30);

        textField = new JTextField();

        textField.setBounds(150,50,170,30);

        textArea  = new JTextArea();
        textArea.setBounds(40,180,300,150);

        add(button); add(labelB);
        add(labelT);add(textField);add(textArea);

    }


    public static void main(String[] args) {
        new Exercise3();
    }
}
