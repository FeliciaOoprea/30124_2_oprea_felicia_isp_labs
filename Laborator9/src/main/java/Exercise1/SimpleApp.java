package Exercise1;

import javax.swing.*;

public class SimpleApp extends JFrame {
    SimpleApp() {
        setTitle("title");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(400,500);
        setVisible(true);
    }

    public static void main(String[] args) {
        SimpleApp simpleApp = new SimpleApp();
    }
}
