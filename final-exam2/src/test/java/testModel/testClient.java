package testModel;

import Model.Client;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class testClient {
    @Test
    void testConstructorOnlyWithString() {
        String s = "1/Oprea Felicia/felicia.oprea@gmail.com";
        Client client  = new Client(s);
        assertEquals(1, client.getID());
        assertEquals("Oprea Felicia",client.getName());
        assertEquals("felicia.oprea@gmail.com",client.getEmail());

    }

    void testToString(){
        Client client = new Client(1,"Oprea Felicia", "felicia.oprea@gmail.com");
        assertEquals("1/Oprea Felicia/felicia.oprea@gmail.com",client.toString());
    }


}
