package testModel;

import Model.Loan;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

public class testLoan {
    @Test
    void testConstructorOnlyWithString(){
        String s = "1/1/1/false";
        Loan loan = new Loan(s);
        assertEquals(1,loan.getID());
        assertEquals(1,loan.getIdClient());
        assertEquals(1,loan.getIdBook());
        assertFalse(loan.isReturned());

    }

    @Test
    void testToString(){
        Loan loan = new Loan(1,1,1,false);
        assertEquals("1/1/1/false",loan.toString());
    }
}
