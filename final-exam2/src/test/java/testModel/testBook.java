package testModel;

import Model.Book;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class testBook {
    @Test
    void testConstructorOnlyWithString(){
        String s = "1/Crime and Punishment/Feodor Dostoevsky/0/0/5";
        Book book = new Book(s);
        assertEquals(1,book.getID());
        assertEquals("Crime and Punishment",book.getTitle());
        assertEquals("Feodor Dostoevsky",book.getAuthorName());
        assertEquals(0,book.getNrStars());
        assertEquals(0,book.getNrReviews());
        assertEquals(5,book.getNrOfBooks());
    }

    @Test
    void testGetReview(){
        Book book = new Book(1,"Crime and Punishment","Feodor Dostoevsky",20,2,5);
        assertEquals(10,book.getReview());
    }

    @Test
    void testToString(){
        Book book = new Book(1,"Crime and Punishment","Feodor Dostoevsky",20,2,5);
        assertEquals("1/Crime and Punishment/Feodor Dostoevsky/20/2/5",book.toString());
    }

}
