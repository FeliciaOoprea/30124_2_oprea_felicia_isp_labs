package Model;

import java.util.Objects;

public class Client {
    private int ID;
    private String name;
    private String email;


    public Client(int ID, String name, String email) {
        this.ID = ID;
        this.name = name;
        this.email = email;
    }

    public Client(String s) {
        try {
            String[] client_split = s.split("/");
            int id = Integer.parseInt(client_split[0]);
            String name = client_split[1];
            String email = client_split[2];
            this.name = name;
            this.email = email;
            this.ID = id;
        }
        catch (RuntimeException e) {
            System.out.println(e.getMessage());
        }
    }

    public int getID() {
        return ID;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Client client = (Client) o;
        return Objects.equals(name, client.name) && Objects.equals(email, client.email);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, email);
    }

    @Override
    public String toString() {
        return ID +
                "/" + name +
                "/" + email ;

    }

    public String toShow(){
        return String.format("|%-10d|%-60s|%-80s\n", getID(),getName(),getEmail());

    }
}
