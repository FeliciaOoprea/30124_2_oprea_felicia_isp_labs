package Model;

import java.util.Objects;

public class Book {
    private int ID;
    private String title;
    private String authorName;
    private int nrStars = 0;
    private int nrReviews = 0;
    private int nrOfBooks;

    public Book(int ID, String title, String authorName, int nrOfBooks) {
        this.ID = ID;
        this.title = title;
        this.authorName = authorName;
        this.nrOfBooks = nrOfBooks;
    }

    public Book(int ID, String title, String authorName, int nrStars, int nrReviews, int nrOfBooks) {
        this.ID = ID;
        this.title = title;
        this.authorName = authorName;
        this.nrStars = nrStars;
        this.nrReviews = nrReviews;
        this.nrOfBooks = nrOfBooks;
    }

    public Book(String s) {
        try{
            String[] book_slit = s.split("/");
            int id = Integer.parseInt(book_slit[0]);
            String title = book_slit[1];
            String author = book_slit[2];
            int nrStars = Integer.parseInt(book_slit[3]);
            int nrReviews = Integer.parseInt(book_slit[4]);
            int nrOfBooks = Integer.parseInt(book_slit[5]);
            this.ID = id;
            this.title = title;
            this.authorName = author;
            this.nrStars = nrStars;
            this.nrReviews = nrReviews;
            this.nrOfBooks = nrOfBooks;
        }
        catch (RuntimeException e) {
            System.out.println(e.getMessage());
        }


    }

    public String getTitle() {
        return title;
    }

    public String getAuthorName() {
        return authorName;
    }

    public int getNrOfBooks() {
        return nrOfBooks;
    }

    public void setNrOfBooks(int nrOfBooks) {
        this.nrOfBooks = nrOfBooks;
    }

    public int getID() {
        return ID;
    }


    public float getReview(){

        if (getNrStars() > 0 & getNrReviews() > 0) return (float) nrStars/nrReviews;
        return 0;
    }

    public void setNrStars(int nrStars) {
        this.nrStars = nrStars;
    }

    public int getNrStars() {
        return nrStars;
    }

    public int getNrReviews() {
        return nrReviews;
    }

    public void setNrReviews(int nrReviews) {
        this.nrReviews = nrReviews;
    }

    public String toString() {
        return ID +
                "/" + title +
                "/" + authorName +
                "/" + nrStars +
                "/" + nrReviews +
                "/" + nrOfBooks ;
    }

    public String toShow() {
        return String.format("|%-8d|%-30.30s|%-30.30s|%-20d|%-1.2f ✨|\n",ID,title,authorName,nrOfBooks,getReview());

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Book book = (Book) o;
        return  Objects.equals(title, book.title) && Objects.equals(authorName, book.authorName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(title, authorName, nrStars, nrReviews, nrOfBooks);
    }
}
