package Controller;


import Model.Book;
import Model.Client;
import Model.Loan;
import Repository.BooksRepository;
import Repository.ClientsRepository;
import Repository.LoansRepository;
import View.LibrarianInterface;

import javax.swing.*;

public class ControllerLibrarian extends JFrame {
    private final LibrarianInterface librarianInterface;
    private final ClientsRepository clientsRepository;
    private final BooksRepository booksRepository;
    private final LoansRepository loansRepository;

    public ControllerLibrarian(LibrarianInterface lb_interface, ClientsRepository cl_repo,
                               BooksRepository bk_repo, LoansRepository ln_repo) {

        this.librarianInterface = lb_interface;
        this.clientsRepository = cl_repo;
        this.booksRepository = bk_repo;
        this.loansRepository = ln_repo;

    }

    private int getIdBook() {
        int k = 0;
        for (Book book : this.booksRepository.getBooks()) {
            if (book.getID() >= k) {
                k = book.getID();
            }
        }
        return k + 1;
    }


    private Book getBook(int id) {
        for (Book book : booksRepository.getBooks()) {
            if (book.getID() == id) return book;
        }
        return null;
    }

    private Book getBook(Book newBook) {
        for (Book book : booksRepository.getBooks()) {
            if (book.equals(newBook)) return book;
        }
        return null;
    }

    private Loan getLoan(int idBook) {
        for (Loan loan : loansRepository.getLoans()) {
            if (loan.getIdBook() == idBook) return loan;
        }
        return null;
    }

    private void showBorrowedBooks() {
        String borrowedBookToShow = "";
        librarianInterface.getBorrowedBooksTextArea().setText("");
        for (Loan loan : loansRepository.getLoans()) {
            borrowedBookToShow += loan.toShow();
        }
        librarianInterface.getBorrowedBooksTextArea().setText(borrowedBookToShow);
    }

    private void showClients() {
        String clientsToShow = "";
        librarianInterface.getClientTextArea().setText("");
        for (Client client : clientsRepository.getClients()) {
            clientsToShow += client.toShow();
        }
        librarianInterface.getClientTextArea().setText(clientsToShow);
    }


    public void runControllerLibrarian() {
        librarianInterface.getAddBook().addActionListener(e -> {
            String title;
            String author;
            int quantity;
            try {
                title = librarianInterface.getTitleTextField().getText();
                author = librarianInterface.getAuthorTextField().getText();
                quantity = Integer.parseInt(librarianInterface.getNrOfBooksTextField().getText());
                Book book = new Book(this.getIdBook(), title, author, quantity);
                Book book1 = getBook(book);
                if (book1 != null) {
                    book1.setNrOfBooks(book1.getNrOfBooks() + quantity);
                    librarianInterface.getAddMessageLabel().setText("The book already exists! The quantity has been updated!");
                } else {
                    this.booksRepository.add(book);
                    librarianInterface.getAddMessageLabel().setText("The book was added successfully!");
                }
                this.booksRepository.updateFile();
            } catch (RuntimeException er) {
                librarianInterface.getAddMessageLabel().setText("Invalid input data! Please try again!");
            }


        });

        librarianInterface.getDeleteBook().addActionListener(e -> {
            librarianInterface.getDeleteMessageLabel().setText("");
            try {
                int id = Integer.parseInt(librarianInterface.getIdTextField().getText());
                Book book = getBook(id);
                Loan loan = getLoan(id);
                if (loan != null) {
                    librarianInterface.getDeleteMessageLabel().setText("This book cannot be deleted!");
                } else {
                    try {
                        this.booksRepository.delete(book);
                        this.booksRepository.updateFile();
                        librarianInterface.getDeleteMessageLabel().setText("The book was deleted successfully!");
                    } catch (RuntimeException er) {
                        librarianInterface.getDeleteMessageLabel().setText(er.getMessage());
                    }
                }
            } catch (RuntimeException er) {
                librarianInterface.getDeleteMessageLabel().setText("Invalid ID. Please try again!");
            }

        });


        librarianInterface.getViewClients().addActionListener(e -> {
            showClients();
        });

        librarianInterface.getHideClients().addActionListener(e -> {
            librarianInterface.getClientTextArea().setText("");
        });

        librarianInterface.getViewBorrowedBooks().addActionListener(e -> {
            showBorrowedBooks();
        });

        librarianInterface.getHideBorrowedBooks().addActionListener(e -> {
            librarianInterface.getBorrowedBooksTextArea().setText("");
        });

    }
}
