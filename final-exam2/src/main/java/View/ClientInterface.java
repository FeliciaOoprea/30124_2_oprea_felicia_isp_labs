package View;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;

public class ClientInterface extends JFrame {
    private JTextField nameTextField;
    private JTextField emailTextField;
    private JButton addClientButton;
    private JLabel newClientMessageLabel;

    private JButton viewBooks;
    private JTextArea bookTextArea;

    private JTextField idBookForBorrowTextField;
    private JTextField idClientForBorrowTextField;
    private JButton borrowButton;
    private JLabel borrowMessageLabel;

    private JTextField idBookForReturnTextField;
    private JTextField idClientForReturnTextField;
    private JButton returnButton;
    private JLabel returnMessageLabel;

    private JTextField reviewTextField;
    private JButton leaveReviewButton;
    private JLabel reviewLabel;

    public ClientInterface() {
    }

    public void runClientInterface() {
        setTitle("Client Home Page");
        setVisible(true);
        setSize(600, 900);
        setLocation(1200, 10);
        setResizable(false);
        this.repaint();
        //this.setLayout(null);
        Border raisedBevelBorder = BorderFactory.createRaisedBevelBorder();
        Border loweredBevelBorder = BorderFactory.createLoweredBevelBorder();
        Font myFont = new Font("myFont", Font.PLAIN, 20);

        JLabel welcomeLabel = new JLabel();
        welcomeLabel.setBounds(20, 20, 650, 170);
        welcomeLabel.setBorder(BorderFactory.createCompoundBorder(raisedBevelBorder, loweredBevelBorder));
        add(welcomeLabel);

        JLabel welcomeJLabel = new JLabel("\uD83D\uDCD6 \uD83D\uDCD6    Welcome to our library!    \uD83D\uDCD6 \uD83D\uDCD6");
        welcomeJLabel.setBounds(180, 20, 600, 30);
        welcomeJLabel.setFont(myFont);
        add(welcomeJLabel);

        JLabel newClientLabel = new JLabel("New client? Please make an account!");
        newClientLabel.setBounds(200, 60, 600, 20);
        newClientLabel.setFont(myFont);
        add(newClientLabel);

        JLabel nameLabel = new JLabel("Name:");
        nameLabel.setBounds(40, 100, 100, 20);
        nameLabel.setFont(myFont);
        add(nameLabel);

        nameTextField = new JTextField();
        nameTextField.setBounds(120, 90, 300, 30);
        nameTextField.setFont(myFont);
        add(nameTextField);

        JLabel emailLabel = new JLabel("Email:");
        emailLabel.setBounds(40, 130, 100, 20);
        emailLabel.setFont(myFont);
        add(emailLabel);

        emailTextField = new JTextField();
        emailTextField.setBounds(120, 120, 300, 30);
        emailTextField.setFont(myFont);
        add(emailTextField);

        newClientMessageLabel = new JLabel("");
        newClientMessageLabel.setBounds(150, 160, 600, 20);
        newClientMessageLabel.setFont(myFont);
        add(newClientMessageLabel);

        addClientButton = new JButton("Create account");
        addClientButton.setBounds(440, 105, 200, 30);
        addClientButton.setFont(myFont);
        add(addClientButton);


        JLabel viewBooksLabel = new JLabel();
        viewBooksLabel.setBounds(20, 200, 650, 360);
        viewBooksLabel.setBorder(BorderFactory.createCompoundBorder(raisedBevelBorder, loweredBevelBorder));
        add(viewBooksLabel);

        bookTextArea = new JTextArea();
//       JScrollPane jScrollPane = new JScrollPane(bookTextArea,JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
//        bookTextArea.setLineWrap(true);
//        bookTextArea.setWrapStyleWord(true);
        bookTextArea.setBounds(40, 250, 610, 250);
        bookTextArea.setFont(new Font("Font", Font.PLAIN, 15));
//        jScrollPane.setPreferredSize(new Dimension(200, 250));
//        add(jScrollPane,BorderLayout.CENTER);
        add(bookTextArea);

        JLabel formatLabel = new JLabel("ID     Title                    Author                   Quantity         Review");
        formatLabel.setFont(myFont);
        formatLabel.setBounds(50, 210, 610, 40);
        add(formatLabel);

        viewBooks = new JButton("View all the books from the library");
        viewBooks.setBounds(140, 510, 400, 30);
        viewBooks.setFont(myFont);
        add(viewBooks);


        JLabel borrowLabel = new JLabel();
        borrowLabel.setBounds(20, 570, 650, 130);
        borrowLabel.setBorder(BorderFactory.createCompoundBorder(raisedBevelBorder, loweredBevelBorder));
        add(borrowLabel);

        JLabel idClientForBorrowLabel = new JLabel("ID Client:");
        idClientForBorrowLabel.setBounds(150, 590, 100, 20);
        idClientForBorrowLabel.setFont(myFont);
        add(idClientForBorrowLabel);

        idClientForBorrowTextField = new JTextField();
        idClientForBorrowTextField.setBounds(250, 580, 70, 30);
        idClientForBorrowTextField.setFont(myFont);
        add(idClientForBorrowTextField);

        JLabel idBookForBorrowLabel = new JLabel("ID Book:");
        idBookForBorrowLabel.setBounds(390, 590, 100, 20);
        idBookForBorrowLabel.setFont(myFont);
        add(idBookForBorrowLabel);

        idBookForBorrowTextField = new JTextField();
        idBookForBorrowTextField.setBounds(490, 580, 70, 30);
        idBookForBorrowTextField.setFont(myFont);
        add(idBookForBorrowTextField);

        borrowButton = new JButton("Borrow book");
        borrowButton.setBounds(270, 620, 150, 30);
        borrowButton.setFont(myFont);
        borrowButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        add(borrowButton);

        borrowMessageLabel = new JLabel("");
        borrowMessageLabel.setBounds(40, 660, 600, 30);
        borrowMessageLabel.setFont(myFont);
        borrowMessageLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        add(borrowMessageLabel);

        JLabel returnLabel = new JLabel();
        returnLabel.setBounds(20, 710, 650, 230);
        returnLabel.setBorder(BorderFactory.createCompoundBorder(raisedBevelBorder, loweredBevelBorder));
        add(returnLabel);

        JLabel idClientForReturnLabel = new JLabel("ID Client:");
        idClientForReturnLabel.setBounds(150, 730, 100, 20);
        idClientForReturnLabel.setFont(myFont);
        add(idClientForReturnLabel);

        idClientForReturnTextField = new JTextField();
        idClientForReturnTextField.setBounds(250, 720, 70, 30);
        idClientForReturnTextField.setFont(myFont);
        add(idClientForReturnTextField);

        JLabel idBookForReturnLabel = new JLabel("ID Book:");
        idBookForReturnLabel.setBounds(390, 730, 100, 20);
        idBookForReturnLabel.setFont(myFont);
        add(idBookForReturnLabel);

        idBookForReturnTextField = new JTextField();
        idBookForReturnTextField.setBounds(490, 720, 70, 30);
        idBookForReturnTextField.setFont(myFont);
        add(idBookForReturnTextField);

        returnButton = new JButton("Return book");
        returnButton.setBounds(270, 770, 150, 30);
        returnButton.setFont(myFont);
        returnButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        add(returnButton);

        returnMessageLabel = new JLabel("");
        returnMessageLabel.setBounds(40, 810, 600, 20);
        returnMessageLabel.setFont(myFont);
        returnMessageLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        add(returnMessageLabel);

        JLabel reviewMessageLabel = new JLabel("<html>If you enjoyed this book, please leave a review. How many stars do<br> you give it? (0-10) ✨</html> ");
        reviewMessageLabel.setBounds(40, 840, 600, 50);
        reviewMessageLabel.setFont(myFont);
        add(reviewMessageLabel);

        reviewTextField = new JTextField();
        reviewTextField.setBounds(250, 870, 80, 30);
        reviewTextField.setFont(myFont);
        add(reviewTextField);

        leaveReviewButton = new JButton("Leave review");
        leaveReviewButton.setBounds(350, 870, 150, 30);
        leaveReviewButton.setFont(myFont);
        add(leaveReviewButton);

        JLabel bookReviewLabel = new JLabel("Book review:");
        bookReviewLabel.setBounds(40, 900, 150, 30);
        bookReviewLabel.setFont(myFont);
        add(bookReviewLabel);

        reviewLabel = new JLabel("");
        reviewLabel.setBounds(170, 900, 450, 30);
        reviewLabel.setFont(myFont);
        add(reviewLabel);


    }

    public JLabel getNewClientMessageLabel() {
        return newClientMessageLabel;
    }

    public JButton getViewBooks() {
        return viewBooks;
    }

    public JButton getReturnButton() {
        return returnButton;
    }

    public JTextField getReviewTextField() {
        return reviewTextField;
    }

    public JTextField getIdClientForReturnTextField() {
        return idClientForReturnTextField;
    }

    public JTextField getIdBookForReturnTextField() {
        return idBookForReturnTextField;
    }

    public JTextArea getBookTextArea() {
        return bookTextArea;
    }

    public JLabel getReviewLabel() {
        return reviewLabel;
    }

    public JButton getBorrowButton() {
        return borrowButton;
    }

    public JTextField getIdClientForBorrowTextField() {
        return idClientForBorrowTextField;
    }

    public JTextField getIdBookForBorrowTextField() {
        return idBookForBorrowTextField;
    }

    public JButton getAddClientButton() {
        return addClientButton;
    }

    public JButton getLeaveReviewButton() {
        return leaveReviewButton;
    }

    public JTextField getNameTextField() {
        return nameTextField;
    }

    public JTextField getEmailTextField() {
        return emailTextField;
    }

    public JLabel getReturnMessageLabel() {
        return returnMessageLabel;
    }

    public JLabel getBorrowMessageLabel() {
        return borrowMessageLabel;
    }


}
