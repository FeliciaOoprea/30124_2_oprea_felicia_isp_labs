package View;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;


public class LibraryInterface extends javax.swing.JFrame {
    private JTextField username;
    private JButton hintUsername;
    private JPasswordField password;
    private JButton hintPassword;
    private JButton clientButton;
    private JButton loginButton;

    public LibraryInterface() {
        setTitle("Library management application");
        setLayout(null);
        setSize(500, 700);
        setLocation(10, 10);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setResizable(false);
        init();
        setVisible(true);
        this.repaint();
    }

    public void init() {
        Border raisedBevelBorder = BorderFactory.createRaisedBevelBorder();
        Border loweredBevelBorder = BorderFactory.createLoweredBevelBorder();
        Font myFont = new Font("myFont", Font.PLAIN, 20);
        Font iconFont = new Font("iconFont", Font.PLAIN, 100);

        JLabel labelBack = new JLabel("\uD83D\uDD6E \uD83D\uDD6E \uD83D\uDD6E \uD83D\uDD6E");
        labelBack.setFont(iconFont);
        labelBack.setForeground(Color.GRAY);
        labelBack.setBounds(10, 60, 450, 120);
        labelBack.setBorder(BorderFactory.createCompoundBorder(raisedBevelBorder, loweredBevelBorder));
        add(labelBack);

        JLabel libraryJLabel = new JLabel("LIBRARY MANAGEMENT PROJECT");
        libraryJLabel.setBounds(30, 70, 450, 100);
        libraryJLabel.setFont(new Font("Font", Font.BOLD, 25));
        add(libraryJLabel);

        JLabel label = new JLabel("");
        label.setBounds(10, 210, 450, 270);
        label.setBorder(BorderFactory.createCompoundBorder(raisedBevelBorder, loweredBevelBorder));
        add(label);

        JLabel librarianLabel = new JLabel("\uD83D\uDCDA    Librarian login   \uD83D\uDCDA");
        librarianLabel.setBounds(120, 220, 220, 50);
        librarianLabel.setFont(myFont);
        add(librarianLabel);

        username = new JTextField("Username");
        username.setBounds(40, 280, 200, 50);
        username.setBorder(BorderFactory.createCompoundBorder(raisedBevelBorder, loweredBevelBorder));
        username.setFont(myFont);
        add(username);

        hintUsername = new JButton("Need a hint? \uD83E\uDD14");
        hintUsername.setBounds(260, 280, 170, 50);
        hintUsername.setFont(myFont);
        add(hintUsername);

        password = new JPasswordField("Password", 9);
        password.setBounds(40, 340, 200, 50);
        password.setBorder(BorderFactory.createCompoundBorder(raisedBevelBorder, loweredBevelBorder));
        password.setFont(myFont);
        add(password);

        hintPassword = new JButton("Need a hint?\uD83E\uDD14");
        hintPassword.setBounds(260, 340, 170, 50);
        hintPassword.setFont(myFont);
        add(hintPassword);

        loginButton = new JButton("Login");
        loginButton.setBounds(130, 420, 200, 50);
        loginButton.setBorder(BorderFactory.createCompoundBorder(raisedBevelBorder, loweredBevelBorder));
        loginButton.setFont(myFont);
        add(loginButton);

        clientButton = new JButton("Client home page");
        clientButton.setBounds(130, 530, 200, 70);
        clientButton.setFont(new Font("Font", Font.PLAIN, 15));
        clientButton.setBorder(BorderFactory.createCompoundBorder(raisedBevelBorder, loweredBevelBorder));
        clientButton.setFont(myFont);
        add(clientButton);

    }

    public JTextField getUsername() {
        return username;
    }

    public JPasswordField getPassword() {
        return password;
    }

    public JButton getClientButton() {
        return clientButton;
    }

    public JButton getLoginButton() {
        return loginButton;
    }

    public JButton getHintUsername() {
        return hintUsername;
    }

    public JButton getHintPassword() {
        return hintPassword;
    }


}