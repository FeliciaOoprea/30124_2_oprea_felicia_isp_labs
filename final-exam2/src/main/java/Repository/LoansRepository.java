package Repository;

import Exceptions.InvalidLoanException;
import Model.Loan;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;


public class LoansRepository {
    private final String fileName;
    private final ArrayList<Loan> loans;

    public LoansRepository() {
        this.fileName = "src/main/resources/Loans.txt";
        this.loans = new ArrayList<>();
        this.readFromFile();
    }

    public ArrayList<Loan> getLoans() {
        return loans;
    }

    public void saveToFile() {
        try {
            FileWriter fileWriter = new FileWriter(this.fileName, false);
            for (Loan loan : loans) {
                fileWriter.write(loan.toString());
                fileWriter.write("\n");
            }
            fileWriter.close();
        } catch (IOException e) {
            throw new RuntimeException(
                    String.format("An error occurred while saving to file: %s", e.getMessage()),
                    e
            );
        }
    }

    public void readFromFile() {
        try {
            List<String> all_lines = Files.readAllLines(Paths.get(fileName));
            for (String line : all_lines) {
                Loan loan = new Loan(line);
                loans.add(loan);
            }
        } catch (IOException e) {
            //ignore missing file
        }
    }

    public void add(Loan loan) {
        if (!verify_if_in_file(loan)) {
            loans.add(loan);
            this.saveToFile();
        } else throw new InvalidLoanException("This loan already exists!");

    }

    public boolean verify_if_in_file(Loan loan) {
        for (Loan loan1 : this.loans) {
            if (loan1.equals(loan)) return true;
        }
        return false;
    }

    public void updateFile() {
        this.saveToFile();
    }

    public void delete(Loan loan) {
        if (verify_if_in_file(loan)) {
            this.loans.remove(loan);
            saveToFile();
        } else throw new InvalidLoanException("This loan does not exist!");
    }
}
