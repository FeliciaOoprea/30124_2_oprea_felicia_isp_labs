package Repository;

import Exceptions.InvalidBookException;
import Model.Book;


import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;


public class BooksRepository {
    private final String fileName;
    private final ArrayList<Book> books;

    public BooksRepository(){
        this.fileName = "src/main/resources/Books.txt";
        this.books = new ArrayList<>();
        this.readFromFile();
    }

    public ArrayList<Book> getBooks() {
        return books;
    }

    public void saveToFile(){
        try{
            FileWriter fileWriter = new FileWriter(this.fileName,false);
            for (Book book : books) {
                fileWriter.write(book.toString());
                fileWriter.write("\n");
            }
            fileWriter.close();
        } catch (IOException e) {
            throw new RuntimeException(
                    String.format("An error occurred while saving to file: %s",e.getMessage()),e
            );
        }
    }

    public void readFromFile(){
        try {
            List<String> all_lines = Files.readAllLines(Paths.get(this.fileName));
            for (String line : all_lines) {
                Book book = new Book(line);
                books.add(book);
            }
        }
        catch (IOException e ){
            //ignore missing file
        }
    }

    public void add(Book book) {
        if (!verify_if_in_file(book)) {
            this.books.add(book);
            this.saveToFile();
        }
        else throw new InvalidBookException("This book already exists!");
    }

    public boolean verify_if_in_file(Book book) {
        for (Book book1 : this.books) {
            if (book1.equals(book)) return true;
        }
        return false;
    }

    public void updateFile() {
        this.saveToFile();
    }

    public void delete(Book book) {
        if (verify_if_in_file(book)) {
            this.books.remove(book);
            saveToFile();
        }
        else throw new InvalidBookException("This book does not exist!");
    }
}

