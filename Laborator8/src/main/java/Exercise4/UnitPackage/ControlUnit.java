package Exercise4.UnitPackage;

import Exercise4.EventPackage.Event;
import Exercise4.EventPackage.EventType;
import Exercise4.EventPackage.FireEvent;
import Exercise4.EventPackage.TemperatureEvent;
import Exercise4.SensorPackage.FireSensor;
import Exercise4.SensorPackage.TempSensor;

import java.io.FileWriter;
import java.io.IOException;

public class ControlUnit {
    private static volatile ControlUnit instance = null;
    private ControlUnit(){};
    public static ControlUnit getInstance(){
        synchronized (ControlUnit.class){
            if (instance == null) {
                instance = new ControlUnit();
            }
        }
        return instance;
    }

    public static void controlUnitHouse(Event event, FileWriter fileWriter) throws IOException {
        if (event.getType() == EventType.TEMPERATURE){
            TempSensor tempSensor = TempSensor.getTempSensor();
            fileWriter.write(tempSensor.activate((TemperatureEvent) event,25));
            fileWriter.write("-----------------------------------------------\n");
        }
        else if (event.getType()==EventType.FIRE) {
            FireSensor fireSensor = new FireSensor();
            fileWriter.write(fireSensor.activate((FireEvent) event));
            fileWriter.write("-----------------------------------------------\n");

        }
        else {
            fileWriter.write("There are no new events at the house!\n");
            fileWriter.write("-----------------------------------------------\n");
        }
        fileWriter.flush();
    }
}
