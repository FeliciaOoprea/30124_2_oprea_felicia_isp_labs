package Exercise4.SensorPackage;

import Exercise4.EventPackage.FireEvent;
import Exercise4.UnitPackage.AlarmUnit;
import Exercise4.UnitPackage.GsmUnit;

public class FireSensor {
    public String activate(FireEvent fireEvent){
        if (fireEvent.isSmoke()){
            return "        Fire sensor is activate:         \n" +
                    new AlarmUnit().startAlarm()+ '\n'+
                    new GsmUnit().callOwner()+"\n";

        }
        else return "       There is no fire.       \n";
    }
}
