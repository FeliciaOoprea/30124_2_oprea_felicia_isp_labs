package Exercise4.SensorPackage;

import Exercise4.EventPackage.TemperatureEvent;
import Exercise4.UnitPackage.CoolingUnit;
import Exercise4.UnitPackage.HeatingUnit;

public class TempSensor {
    public static TempSensor tempSensor = new TempSensor() ;
    public static TempSensor getTempSensor(){
        return tempSensor;
    }

    public String activate(TemperatureEvent temperatureEvent, int PRESET_VALUE){
        if (temperatureEvent.getVlaue() > PRESET_VALUE) {
            return "        Temperature is bigger than " + PRESET_VALUE + " degrees: \n" +
                    new CoolingUnit().startCoolingUnit() + "        \n";
        }
        else {
            return "        Temperature is lower than " + PRESET_VALUE + "degrees: \n" +
                    new HeatingUnit().startHeatingUnit() + "        \n";
        }
    }
}
