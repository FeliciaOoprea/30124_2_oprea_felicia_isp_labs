package Exercise4.EventPackage;

import Exercise4.EventPackage.Event;
import Exercise4.EventPackage.EventType;

public class FireEvent extends Event {

    final private boolean smoke;

    public FireEvent(boolean smoke) {
        super(EventType.FIRE);
        this.smoke = smoke;
    }

    public boolean isSmoke() {
        return smoke;
    }

    @Override
    public String toString() {
        return "FireEvent{" + "smoke=" + smoke + '}';
    }
}
