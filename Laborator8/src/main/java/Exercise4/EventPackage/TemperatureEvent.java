package Exercise4.EventPackage;

import Exercise4.EventPackage.Event;
import Exercise4.EventPackage.EventType;

public class TemperatureEvent extends Event {
    private int vlaue;

    public TemperatureEvent(int vlaue) {
        super(EventType.FIRE.TEMPERATURE);
        this.vlaue = vlaue;
    }

    public int getVlaue() {
        return vlaue;
    }

    @Override
    public String toString() {
        return "TemperatureEvent{" + "vlaue=" + vlaue + '}';
    }
}
