package Exercise4.EventPackage;

public class NoEvent extends Event {
    public NoEvent() {
        super(EventType.NONE);
    }

    @Override
    public String toString() {
        return "NoEvent{}";
    }
}

