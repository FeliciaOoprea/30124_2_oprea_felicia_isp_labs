package Exercise4.EventPackage;

import Exercise4.EventPackage.EventType;

public class Event {
    EventType type;

    Event(EventType type) {
        this.type = type;
    }

    public EventType getType() {
        return type;
    }

}
