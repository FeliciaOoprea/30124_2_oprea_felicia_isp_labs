package Exercise3Modified;


public class Simulator {
    public static void main(String[] args) {

        //build station Cluj-Napoca
        Controller c1 = new Controller("Cluj-Napoca");
        Segment s1 = new Segment(1);
        c1.addControlledSegment(s1);

        //build station Bucuresti
        Controller c2 = new Controller("Bucuresti");
        Segment s2 = new Segment(2);
        c2.addControlledSegment(s2);

        //build station Brasov
        Controller c3 = new Controller("Brasov");
        Segment s3 = new Segment(3);
        Segment s4 = new Segment(4);
        c3.addControlledSegment(s3);
        c3.addControlledSegment(s4);

        //connect the 3 controllers

        c1.setNeighbourController(c2);
        c1.setNeighbourController(c3);
        c2.setNeighbourController(c1);
        c2.setNeighbourController(c3);
        c3.setNeighbourController(c1);
        c3.setNeighbourController(c2);

        //testing
        //Controller 3 is connected with controller 1 and 2, 2 trains from Brasov leave to Cluj-Napoca and Bucuresti
        Train t1 = new Train("Bucuresti", "Train-1");
        s3.arriveTrain(t1);

        Train t2 = new Train("Cluj-Napoca","Train-2");
        s4.arriveTrain(t2);

        c1.displayStationState();
        c2.displayStationState();
        c3.displayStationState();

        System.out.println("\nStart train control\n");

        //execute 3 times controller steps
        for(int i = 0;i<3;i++){
            System.out.println("### Step "+i+" ###");
            c1.controlStep();
            c2.controlStep();
            c3.controlStep();

            System.out.println();

            c1.displayStationState();
            c2.displayStationState();
            c3.displayStationState();
        }
    }

}
