package Controller;

import Model.Librarian;
import View.ClientInterface;
import View.LibrarianInterface;
import View.LibraryInterface;

import javax.swing.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class ControllerLibrary {
    private final LibraryInterface libraryInterface;
    private final ControllerClient controllerClient;
    private final ClientInterface clientInterface;
    private final LibrarianInterface librarianInterface;
    private final ControllerLibrarian controllerLibrarian;

    public ControllerLibrary(LibraryInterface lib_interface,
                             ControllerClient cl_controller,
                             ClientInterface cl_int,
                             LibrarianInterface lb_int,
                             ControllerLibrarian lb_controller) {
        this.libraryInterface = lib_interface;
        this.controllerClient = cl_controller;
        this.clientInterface = cl_int;
        this.librarianInterface = lb_int;
        this.controllerLibrarian = lb_controller;
    }

    public void runControllerLibrary() {
        Librarian librarian = new Librarian("Felicia", "final-exam");
        libraryInterface.getPassword().addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                libraryInterface.getPassword().setText("");
            }
        });
        libraryInterface.getUsername().addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                libraryInterface.getUsername().setText("");
            }
        });

        libraryInterface.getHintUsername().addActionListener(e -> {
            String st = "Correct username: Felicia";
            JOptionPane.showMessageDialog(libraryInterface, st);

        });

        libraryInterface.getHintPassword().addActionListener(e -> {
            String st = "Correct password: final-exam";
            JOptionPane.showMessageDialog(libraryInterface, st);
        });


        libraryInterface.getLoginButton().addActionListener(e -> {

            if (libraryInterface.getUsername().getText().equals(librarian.getUsername())
                    && libraryInterface.getPassword().getText().equals(librarian.getPassword())) {
                this.librarianInterface.runLibrarianInterface();
                this.controllerLibrarian.runControllerLibrarian();

            } else {
                String st = "Wrong username or password.";
                JOptionPane.showMessageDialog(libraryInterface, st);
            }
        });

        libraryInterface.getClientButton().addActionListener(e -> {
            this.clientInterface.runClientInterface();
            this.controllerClient.runClientController();
        });
    }

}
