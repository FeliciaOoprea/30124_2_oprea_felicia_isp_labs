package Exercise1;

public class CoffeeMaker {
    private int nrOfCoffees;

    public CoffeeMaker(int nrOfCoffees) {
        this.nrOfCoffees = nrOfCoffees;
    }

    Coffee makeCoffee() throws Exception {
        System.out.println("Make a coffee!");
        int t = (int) (Math.random() * 100);
        int c = (int) (Math.random() * 100);
        if ( Coffee.getNrOfCoffeesMade() == nrOfCoffees) {
            throw new Exception(nrOfCoffees + "coffees were made!");
        }
        Coffee coffee = new Coffee(t, c);
        return coffee;
    }
}
