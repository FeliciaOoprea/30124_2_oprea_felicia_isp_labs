package Exercise1;


public class Main {
    public static void main(String[] args) throws Exception {

        CoffeeMaker coffeeMaker = new CoffeeMaker(5);
        CoffeeDrinker coffeeDrinker = new CoffeeDrinker();
        for (int i = 0; i < 10; i++) {
            Coffee coffee = coffeeMaker.makeCoffee();
            try {
                coffeeDrinker.drinkCoffee(coffee);
            } catch (TemperatureException e) {
                System.out.println("Exception:" + e.getMessage() + " temp=" + e.getTemp());
            } catch (ConcentrationException e) {
                System.out.println("Exception:" + e.getMessage() + " conc=" + e.getConc());
            } finally {
                System.out.println("Throw the coffee cup.\n");
            }
        }
    }
}


