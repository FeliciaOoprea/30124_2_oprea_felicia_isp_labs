package Exercise1;

public class CoffeeDrinker {
    void drinkCoffee(Coffee c) throws TemperatureException, ConcentrationException{
        if(c.getTemp()>60)
            throw new TemperatureException(c.getTemp(),"Coffee is to hot!");
        if(c.getConc()>70)
            throw new ConcentrationException(c.getConc(),"Coffee concentration to high!");
        System.out.println("Drink coffee:"+c);
    }
}



