package Exercise1;

public class Coffee {
    private static int nrOfCoffeesMade;
    private final int temp;
    private final int conc;

    Coffee(int t, int c) {
        temp = t;
        conc = c;
        nrOfCoffeesMade++;
    }

    int getTemp() {
        return temp;
    }

    int getConc() {
        return conc;
    }

    public static int getNrOfCoffeesMade() {
        return nrOfCoffeesMade;
    }

    public String toString() {
        return "[Coffee temperature=" + temp + ":concentration=" + conc + "]";
    }
}
