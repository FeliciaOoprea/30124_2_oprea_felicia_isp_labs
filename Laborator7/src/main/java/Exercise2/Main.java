package Exercise2;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws FileNotFoundException {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the character to count the occurrences of: ");
        String ch;
        ch = scanner.next();
        int count = 0;
        try (BufferedReader in = new BufferedReader(new FileReader("src/main/resources/data.txt"))){
            String s;
            while ((s = in.readLine()) != null){
                for (String letter : s.split("")){
                    if (letter.equals(ch)){
                        count++;
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("The letter " + ch + " appears " + count + " times.");
    }
}
