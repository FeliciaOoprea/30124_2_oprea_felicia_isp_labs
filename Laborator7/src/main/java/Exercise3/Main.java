package Exercise3;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner console = new Scanner(System.in);
        String name;
        int option;
        boolean menu = true;
        while (menu) {
            System.out.println();
            System.out.println("Choose an option:\n 1. Encrypt a file. \n 2. Decrypt a file. \n " +
                    "3. Exit. ");
            option = console.nextInt();
            switch (option) {
                case 1:
                    System.out.println("Name of the file : ");
                    name = "src/main/resources/" + console.next() + ".txt";
                    EncryptFile.encryptAndSaveFile(name );
                    break;

                case 2:
                    System.out.println("Name of the file: ");
                    name = "src/main/resources/" + console.next() + ".enc";
                    DecryptFile.decryptAndSaveFile(name);
                    break;

                case 3:
                    menu = false;
                    break;

                default:
                    System.out.println("Wrong option!");
            }

        }

    }
}
