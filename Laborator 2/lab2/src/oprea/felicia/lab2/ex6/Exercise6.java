package oprea.felicia.lab2.ex6;

import java.util.Scanner;

public class Exercise6 {
    static int factorial(int n){
        if (n==0)
            return 1;
        else {
            return (n*factorial(n-1));
        }
    }

    public static void main(String[] args) {
        int n;
        int factorial1;
        int factorial2;
        Scanner scanner = new Scanner(System.in);
        System.out.println("Introduce a number: ");
        n = scanner.nextInt();
        factorial1=factorial(n);
        factorial2=1;
        for (int i=1; i<=n; i++){
            factorial2=factorial2*i;
        }
        System.out.println("n! using the recursive method: " +factorial1);
        System.out.println("n! using the non-recursive method: " +factorial2 );

    }
}

