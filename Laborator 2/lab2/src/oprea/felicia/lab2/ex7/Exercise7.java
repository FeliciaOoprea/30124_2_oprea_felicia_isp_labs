package oprea.felicia.lab2.ex7;

import java.util.Random;
import java.util.Scanner;

public class Exercise7 {
    public static void main(String[] args) {
        int n;
        int i;
        Random rd = new Random();
        int rand = rd.nextInt(100);
        Scanner scanner = new Scanner(System.in);
        i = 0;
        while (i < 3) {
            System.out.println("Introduce a number: ");
            n = scanner.nextInt();
            if (rand == n) {
                System.out.println("The number was guessed!");
                break;
            } else if (n < rand) {
                System.out.println("Wrong answer, your number it too low!");
            } else if (n > rand) {
                System.out.println("Wrong answer, your number it too high!");
            }
            i++;
        }
    }
}
