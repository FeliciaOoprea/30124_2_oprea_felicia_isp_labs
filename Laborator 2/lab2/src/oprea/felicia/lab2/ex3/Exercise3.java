package oprea.felicia.lab2.ex3;


import java.util.Scanner;

public class Exercise3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("a=");
        int a = scanner.nextInt();
        System.out.println("b=");
        int b = scanner.nextInt();
        int n = 0;
        int prime;
        System.out.print("Prime numbers between a and b: ");
        for (int i = a; i <= b; i = i + 1) {
            prime = 1;
            for (int j = 2; j <= i / 2; j = j + 1) {
                if (i % j == 0) {
                    prime = 0;
                    break;
                }
            }
            if (prime == 1) {
                n++;
                System.out.println(" " +i);
            }
        }
        System.out.println("Numbers of prime numbers between a and b: " +n);
    }
}
