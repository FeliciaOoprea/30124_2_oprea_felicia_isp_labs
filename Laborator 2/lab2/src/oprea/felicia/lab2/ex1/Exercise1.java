package oprea.felicia.lab2.ex1;

import java.util.Scanner;

public class Exercise1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("First number=");
        int firstNumber = scanner.nextInt();
        System.out.println("Second number=");
        int secondNumber = scanner.nextInt();
        if (firstNumber >= secondNumber) {
            System.out.println("The maximum number is: " + firstNumber);
        } else {
            System.out.println("The maximum number is: " + secondNumber);
        }
    }
}
