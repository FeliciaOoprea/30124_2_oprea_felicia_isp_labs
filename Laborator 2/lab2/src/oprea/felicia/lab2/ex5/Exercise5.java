package oprea.felicia.lab2.ex5;

import java.util.Random;

public class Exercise5 {
    public static void main(String[] args) {
        int[] v = new int[10];
        int aux;
        int i;
        int j;
        Random rd = new Random();
        System.out.println("The unsorted vector: ");
        for (i = 0; i < v.length; i++) {
            v[i] = rd.nextInt(50);
            System.out.print(" " + v[i]);
        }
        for (i = 0; i < v.length - 1; i++) {
            for (j = 0; j < v.length - i - 1; j++) {
                if (v[j] > v[j + +1]) {
                    aux = v[j];
                    v[j] = v[j + 1];
                    v[j + 1] = aux;

                }
            }

        }
        System.out.println("\n");
        System.out.println("The sorted vector: ");
        for (i = 0; i < v.length; i++) {
            System.out.print(" " + v[i]);
        }
    }
}