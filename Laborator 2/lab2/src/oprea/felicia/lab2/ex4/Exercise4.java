package oprea.felicia.lab2.ex4;

import java.util.Scanner;

public class Exercise4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n, maximum, i;
        System.out.println("n=");
        n = scanner.nextInt();
        int[] v = new int[n];
        for (i = 0; i < n; i++) {
            System.out.printf("v[%d]=", i);
            v[i] = scanner.nextInt();
        }
        maximum = v[0];
        for (i = 1; i < n; i++) {
            if (v[i] > maximum)
                maximum = v[i];
        }
        System.out.println("The maximum is: " + maximum);
    }
}
